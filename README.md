# Training #


### Code Katas ###

A short list of (stolen) Katas that can be used to refine skills

- MadeTech: https://learn.madetech.com/katas/
- Codility: https://app.codility.com/programmers/lessons/1-iterations/binary_gap/ 

### Tech ###

#### AWS ####

- Overview: https://aws.amazon.com/training/course-descriptions/?nc2=sb_tr_to

#### ELK ####

- https://www.elastic.co/

#### Kafka ####

- https://kafka.apache.org/

#### Containers ####

- Docker: https://www.docker.com/play-with-docker
- Kubernetes: https://kubernetes.io/docs/tutorials/kubernetes-basics/
- Openshift: https://www.openshift.com/try

#### Mongo ####

- https://www.mongodb.com/

#### Prometheus ####

- https://prometheus.io/docs/introduction/overview/

#### Spring ####

The spring guides are plentiful, I would stick to those which cover the other techs in this list as well as things like RESTful services, gradle ect.

- https://spring.io/guides#getting-started-guides

#### SQL/Relational DB ####

- https://www.w3schools.com/sql/default.asp

#### Tutorials Point #### 
Tutorials Point has an endless list of tech, I would stick to that which we are more likly to use :-)

- https://www.tutorialspoint.com/tutorialslibrary.htm






